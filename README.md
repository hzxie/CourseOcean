![CourseOcean Logo](https://gitlab.com/hzxie/CourseOcean/-/raw/master/public/img/logo.png)
===================


An IT training service platform targeting mainland China, providing you with the most convenient O2O (online-to-offline) IT training services.

----------

[![Build Status](https://travis-ci.org/hzxie/CourseOcean.svg)](https://travis-ci.org/hzxie/CourseOcean)
